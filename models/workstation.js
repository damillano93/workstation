var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WorkstationSchema = new Schema({
  name: {type:String, required:true},
  type: {type:String, required:true},
  laboratory: {type:String, required:true},
  implement: {type:String, required:true},
});

WorkstationSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Workstation', WorkstationSchema);
