var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan')

var cors = require('cors');
var status = require('./routes/status');

var app = express();
app.use(logger('dev'))
app.use(cors());

var mongoose = require('mongoose');
var dev_db_url = process.env.MONGO_URL
console.log(dev_db_url);
mongoose.connect(dev_db_url);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB conexion error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

var persona = require('./routes/persona');
var ciudad = require('./routes/ciudad');
var type = require('./routes/type');
var workstation = require('./routes/workstation');

app.use('/persona', persona);
app.use('/ciudad', ciudad);
app.use('/type', type);
app.use('/workstation', workstation);

app.use('/', status);
var port = 3002;
app.listen(port, () => {
  console.log('Server in port ' + port);
});
