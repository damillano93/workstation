//importar modelo
var Persona = require('../models/persona');

//funcion create
exports.persona_create = function (req, res) {
    var persona = new Persona(
        req.body
    );

    persona.save(function (err, persona) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: persona._id})
    })
};
//funcion read by id
exports.persona_details = function (req, res) {
    Persona.findById(req.params.id, function (err, persona) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(persona);
    })
};
//funcion read all
exports.persona_all = function (req, res) {
    Persona.find(req.params.id, function (err, persona) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(persona);
    })
};
//funcion update
exports.persona_update = function (req, res) {
    Persona.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, persona) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", persona: persona });
    });
};
//funcion add ciudad
exports.Persona_add_ciudad = function (req, res) {
Persona.findByIdAndUpdate(req.params.id, { $push: { ciudad: req.body.ciudad } }, { new: true }, function (err, Persona) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Persona: Persona });
});
};

//funcion delete
exports.persona_delete = function (req, res) {
    Persona.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};