var express = require('express');
var router = express.Router();

var workstationController = require('../controllers/workstation');

router.get('/:id', workstationController.workstationShow);
router.get('/', workstationController.workstationAll);
router.post('/', workstationController.workstationCreate);
router.put('/:id', workstationController.workstationUpdate);
router.delete('/:id', workstationController.workstationDelete);

module.exports = router;
